import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-component',
  templateUrl: './student-component.component.html',
  styleUrls: ['./student-component.component.css']
})
export class StudentComponentComponent implements OnInit {
  @Input() name='';
  sayHello(){
    alert("Hello "+this.name)
  }
  constructor() { }
 
  ngOnInit(): void {
  }

}
