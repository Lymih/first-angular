import { Component } from "@angular/core";
import { HostListener } from "@angular/core";



@Component({
  selector: 'app-exo',
  templateUrl: './exo.component.html'
})
export class ExoComponent {

onKeyDown(event:KeyboardEvent){
  if (event.key=="ArrowUp"){
    this.increment();
  }
  if (event.key=="ArrowDown"){
    this.decrement();
  }
  if (event.key=="32"){
    this.reset();
  }
}

  counter = 0;
  increment() {
    this.counter++;
  }
  decrement(){
    this.counter--;
  }
  reset(){
    this.counter=0;
  }
  divideByTwo(){
    this.counter=this.counter/2;
  }
  multiplyByPi(){
    this.counter=this.counter*Math.PI
  }
}
