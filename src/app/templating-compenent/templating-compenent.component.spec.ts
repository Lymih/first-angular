import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplatingCompenentComponent } from './templating-compenent.component';

describe('TemplatingCompenentComponent', () => {
  let component: TemplatingCompenentComponent;
  let fixture: ComponentFixture<TemplatingCompenentComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TemplatingCompenentComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplatingCompenentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
