import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ExoComponent } from './exo/exo.component';
import { FirstComponent } from './first.component';
import { TemplatingCompenentComponent } from './templating-compenent/templating-compenent.component';
import { StudentComponentComponent } from './student-component/student-component.component';
import { DogComponentComponent } from './dog-component/dog-component.component';
import { DogListComponent } from './dog-list/dog-list.component';




@NgModule({
  declarations: [
    FirstComponent,
    ExoComponent,
    TemplatingCompenentComponent,
    StudentComponentComponent,
    DogComponentComponent,
    DogListComponent,

  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [DogListComponent]
})
export class AppModule { }
