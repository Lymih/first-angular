import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-templating-compenent',
  templateUrl: './templating-compenent.component.html',
  styleUrls: ['./templating-compenent.component.css']
})
export class TemplatingCompenentComponent implements OnInit {

  students: string[] = ['Jim', 'Serigne', 'Camille.T', 'Mohammad', 'Romain', 'Jason', 'Henrique', 'Tarik', 'Damien', 'Safik', 'Mustafa', 'Camille.A', 'Aurelien', 'Amine']
  modalOpen=false;
 
  constructor() { }

  ngOnInit(): void {
  }
  sayHello(name:string){
    alert("Hello "+name+" !");
  }
  toggleModal():boolean{
    this.modalOpen=!this.modalOpen
    console.log(this.modalOpen)
    return this.modalOpen
  }
}
