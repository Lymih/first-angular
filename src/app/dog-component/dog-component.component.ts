import { Component, Input, OnInit } from '@angular/core';
import { Dog } from '../dog';


@Component({
  selector: 'app-dog-component',
  templateUrl: './dog-component.component.html',
  styleUrls: ['./dog-component.component.css']
})
export class DogComponentComponent implements OnInit {
  @Input() dog?:Dog;

  constructor() { }

  ngOnInit(): void {
  }

}
