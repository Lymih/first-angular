import { Component, OnInit } from '@angular/core';
import { Dog } from '../dog';

@Component({
  selector: 'app-dog-list',
  templateUrl: './dog-list.component.html',
  styleUrls: ['./dog-list.component.css']
})
export class DogListComponent implements OnInit {
  dogs:Dog[]=[
    {id:1,name:"Rex",breed:"Dalmatien",birthDate:new Date("2020-03-02")},
    {id:2,name:"Polly",breed:"Poodle",birthDate:new Date("2021-11-01")},
    {id:3,name:"Nils",breed:"Bulldog",birthDate:new Date("2017-05-13")},
    {id:4,name:"Rudy",breed:"Rottweiler",birthDate:new Date("2019-08-27")},
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
